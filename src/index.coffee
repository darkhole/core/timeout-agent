moment      = require 'moment'
SimpleAgent = require '@dark-hole/simple-agent'

class TimeoutAgent extends SimpleAgent

  constructor : ( uri, opts = {} ) ->

    super uri, opts

    # recovery options
    @_timeout = opts.timeout || 2

  setup : ->
    it = @

    @extendTimeout()
    @tick()

    @socket.on 'exec', ( transaction ) ->
      it.extendTimeout()
      it._action transaction

  extendTimeout : ->
    @timeout = moment().add @_timeout, 'minutes'

  tick : ->
    it = @

    setInterval( ->
      diff = moment().diff it.timeout, 'minutes'
      if diff >= 0
        it.socket.disconnect()
      it.tick()
    , 60000 ).unref()

module.exports = TimeoutAgent

